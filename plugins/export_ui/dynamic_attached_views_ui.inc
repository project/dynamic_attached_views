<?php

/**
 * @file
 * A Ctools Export UI plugin for Dynamic Attached Views sets.
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'attached_views',
  'access' => 'administer dynamic attached views',
  'menu' => array(
    'menu item' => 'dynamic_attached_views',
    'menu title' => 'Dynamic Attached Views',
    'menu description' => 'Administer Dynamic Attached Views.',
  ),
  'use advanced help' => TRUE,
  'advanced help' => array(
    'topic' => 'getting-started'
  ),
  'title singular' => t('view set'),
  'title plural' => t('views sets'),
  'title singular proper' => t('View set'),
  'title plural proper' => t('Views sets'),

  'form' => array(
    'settings' => 'dynamic_attached_views_ctools_settings_form',
    'submit' => 'dynamic_attached_views_ctools_submit_form',
  ),
);

/**
 * Define the preset add/edit form.
 */
function dynamic_attached_views_ctools_settings_form(&$form, &$form_state) {
  ctools_include('dependent');
  drupal_add_css(drupal_get_path('module', 'dynamic_attached_views') . '/plugins/export_ui/dynamic_attached_views.css');
  $view_set = $form_state['item'];

  $form['info']['machine_name']['#description'] = t('A machine name for this view set. Alphanumeric and underscores only.');

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('This title will be displayed when user select between the available views to attach.'),
    '#default_value' => !empty($view_set->settings['title']) ? $view_set->settings['title'] : '',
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('This description will be used in the select box of the available views to attach.'),
    '#default_value' => !empty($view_set->settings['description']) ? $view_set->settings['description'] : '',
  );

  $form['fieldset_wrap'] = array(
    '#type' => 'checkbox',
    '#title' => t('Wrap field inside a fieldset'),
    '#default_value' => !empty($view_set->settings['fieldset']['wrap']) ? $view_set->settings['fieldset']['wrap'] : FALSE,
  );
  $form['fieldset_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Fieldset title'),
    '#description' => t('This title will be displayed when user select between the available views to attach.'),
    '#default_value' => !empty($view_set->settings['fieldset']['title']) ? $view_set->settings['fieldset']['title'] : '',
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array(
      'edit-fieldset-wrap' => array(1)
    ),
    '#prefix' => '<div class = "dynamic-attached-views-item-expanded">',
    '#suffix' => '</div>',
  );
  $form['fieldset_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Fieldset description'),
    '#default_value' => !empty($view_set->settings['fieldset']['description']) ? $view_set->settings['fieldset']['description'] : '',
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array(
      'edit-fieldset-wrap' => array(1)
    ),
    '#prefix' => '<div class = "dynamic-attached-views-item-expanded">',
    '#suffix' => '</div>',
  );

  $form['required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Required'),
    '#description' => t('If checked, the select box of the available views to attach will be required.'),
    '#default_value' => !empty($view_set->settings['required']) ? $view_set->settings['required'] : FALSE,
    '#required' => FALSE,
  );

  $form['weight'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow to set weight'),
    '#description' => t('If checked, users may select the position of the view per node.'),
    '#default_value' => !empty($view_set->settings['weight']) ? $view_set->settings['weight'] : FALSE,
    '#required' => FALSE,
  );

  $form['settings']['node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => !empty($view_set->settings['node_types']) ? $view_set->settings['node_types'] : array(),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('Select content types where this view set set will be visible.'),
  );

  $views = views_get_applicable_views('dynamic attach');
  $available_views = array();

  foreach ($views as $data) {
    list($view, $display_id) = $data;
    $description = $view->display_handler->get_option('description');
    $available_views[$view->name . ':' . $view->current_display] = $description . ' [' . t('View: @view Display: @display', array('@view' => $view->name, '@display' => $view->current_display)) . ']';
  }

  $form['settings']['views'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Views'),
    '#default_value' => !empty($view_set->settings['views']) ? $view_set->settings['views'] : array(),
    '#options' => $available_views,
    '#description' => t('Select between which views will the user be able to choose.'),
  );
  if(count($available_views) > 6) {
    $form['settings']['views']['#type'] = 'select';
    $form['settings']['views']['#multiple'] = TRUE;
  }

  if (count($available_views) < 1) {
    drupal_set_message(t('Only views that uses the Dynamic Attached Views display can be selected. Please create at least a view with one those displays.'), 'error');
  }
}

/**
 * Prepare values for saving by ctools export UI.
 */
function dynamic_attached_views_ctools_submit_form($form, &$form_state) {
  $form_state['item']->settings['title'] = $form_state['values']['title'];
  $form_state['item']->settings['description'] = $form_state['values']['description'];
  $form_state['item']->settings['required'] = (bool)$form_state['values']['required'];
  $form_state['item']->settings['weight'] = (bool)$form_state['values']['weight'];
  $form_state['item']->settings['node_types'] = array_filter($form_state['values']['node_types']);
  $form_state['item']->settings['views'] = array_filter($form_state['values']['views']);
  $form_state['item']->settings['fieldset'] = array(
    'wrap' => $form_state['values']['fieldset_wrap'],
    'title' => $form_state['values']['fieldset_title'],
    'description' => $form_state['values']['fieldset_description'],
  );
}
