<?php

class dynamic_attach_views_plugin_display extends views_plugin_display {

  function option_definition() {
    $options = parent::option_definition();
    $options['description'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);

    $categories['dynamic_attach'] = array(
      'title' => t('Dynamic attach settings'),
    );

    $description = $this->get_option('description');
    $options['description'] = array(
      'category' => 'dynamic_attach',
      'title' => t('Description'),
      'value' => !empty($description) ? $description : t('- none -'),
    );
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'description':
        $form['#title'] .= t('Description');
        $description =
        $form['description'] = array(
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => t('Text to display to the user'),
          '#description' => t('Users will see this text when select between all views of the set.'),
          '#default_value' => $this->get_option('description'),
        );
        break;
    }
  }

  function options_submit($form, &$form_state) {
    parent::options_submit($form, $form_state);

    switch ($form_state['section']) {
      case 'description':
        $this->set_option('description', $form_state['values']['description']);
        break;
    }
  }

  function validate() {
    $errors = parent::validate();

    $description = $this->get_option('description');
    if (empty($description)) {
      $errors[] = t('Description for display @display is required.', array('@display' => $this->display->display_title));
    }

    return $errors;
  }
}

