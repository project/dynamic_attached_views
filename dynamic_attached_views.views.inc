<?php

/**
 * Implementation of hook_views_plugins().
 */
function dynamic_attached_views_views_plugins() {
  return array(
    'display' => array(
      'dynamic_attach' => array(
        'title' => t('Dynamic attached'),
        'help' => t('Creates a display specifically for embed with dynamic attached view module.'),
        'handler' => 'dynamic_attach_views_plugin_display',
        'theme' => 'views_view',
        'theme path' => drupal_get_path('module', 'views') . '/theme',
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => TRUE,
        'dynamic attach' => TRUE,
        'accept attachments' => TRUE,
        'help topic' => 'display-default',
      ),
    ),
  );
}
